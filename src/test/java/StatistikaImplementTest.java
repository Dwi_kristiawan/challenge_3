import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class StatistikaImplementTest {
    DecimalFormat df = new DecimalFormat("#.##");
    @BeforeEach
    void setUp() {
        Statiska st=new StatistikaImplement();
    }

    @Test
    void modus() {
        List<List<Integer>>list=new ArrayList<>();
        int x=1;
        for (int i = 5; i > 0; i--) {

                List<Integer>listtmp=new ArrayList<>();
                listtmp.add(x);
                x=x+1;
                listtmp.add(x);
                list.add(listtmp);
                i=i-1;
                x=x+1;

        }

        String modus = null;
        int frekuensi=0;
        for (int i = 0; i <list.size() ; i++) {
            if (frekuensi<Integer.valueOf(list.get(i).get(1))){
                modus=String.valueOf(list.get(i).get(0));
                frekuensi=Integer.valueOf(list.get(i).get(1));
            }
        }
        System.out.println(modus);
        assertEquals("5",modus);
    }

    @Test
    void mean() {
        List<Integer>list=new ArrayList<>();
        for (int i = 1; i <10; i++) {
            list.add(i);
        }
        String mean;
        Double post=0.0;
        for (Integer tmp:list
        ) {
            post=Double.valueOf(tmp)+post;
        }
        mean=String.valueOf(df.format(Double.valueOf(post/list.size())));
        System.out.println(String.valueOf(mean));
        assertEquals("5",mean);
    }

    @Test
    void median() {
        List<Integer>list=new ArrayList<>();
        for (int i = 1; i <6; i++) {
            list.add(i);
        }
        String median;
        if (list.size()%2!=0){
            int temp=((list.size()-1)/2);
            median= String.valueOf(list.get(temp));
        }else{
            median=String.valueOf(Double.valueOf(((list.get((list.size()-1)/2))+((list.get(((list.size()-1)/2)+1)) )/2 )));

        }
        System.out.println(median);
        assertEquals(3,3);

    }

    @Test
    void frekuensi() {
        System.out.println("proses mengelompokan data berhasil");
    }

    @Test
    void inputan(String x) {
        System.out.println("proses memasukan data berhasil");
    }

    @Test
    void shorted() {
        System.out.println("proses mengurutkan data berhasil");

    }

    @Test
    void nilaiInteger() {
        System.out.println("proses konversi nilai keinteger berhasil");
    }
}