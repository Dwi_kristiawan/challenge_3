import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WriteImplement implements WriteFile {

    public void writing(String data, String nama){
        String fileName = "src/dataRecord/"+nama;

        try {
            FileWriter fileWriter = new FileWriter(fileName);
            fileWriter.write(data);

            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Terjadi kesalahan karena: " + e.getMessage());
        }
    }

    @Override
    public void writing(List<List<String>> data, String nama) {
        String fileName = "src/dataRecord/"+nama;

        try {
            FileWriter fileWriter = new FileWriter(fileName);
            List<List<String>>list =new ArrayList<>();
            for (int i = 0; i <data.size(); i++) {
                for (int j = 0; j < data.get(i).size(); j++) {

                    fileWriter.write(data.get(i).get(j)+" ");
                }
                fileWriter.write(" \n");
            }
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Terjadi kesalahan karena: " + e.getMessage());
        }

    }

}
