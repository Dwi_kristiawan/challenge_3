import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class StatistikaImplement implements Statiska {
    //inisialisasi desimal format;
    DecimalFormat df = new DecimalFormat("#.##");

    @Override
    public String modus(List<List<String>> list) {
        String modus = null;
        int frekuensi=0;
        for (int i = 0; i <list.size() ; i++) {
            if (frekuensi<Integer.valueOf(list.get(i).get(1))){
                modus=String.valueOf(list.get(i).get(0));
                frekuensi=Integer.valueOf(list.get(i).get(1));
            }
        }
        return modus;
    }

    @Override
    public String mean(List<Integer> list) {
        String mean;
        Double post=0.0;
        for (Integer tmp:list
             ) {
            post=Double.valueOf(tmp)+post;
        }
        mean=String.valueOf(df.format(Double.valueOf(post/list.size())));

        return mean;


    }

    @Override
    public String median(List<Integer> list) {
        String median;
        if (list.size()%2!=0){
            int temp=((list.size()-1)/2);
            median= String.valueOf(list.get(temp));
        }else{
            median=String.valueOf(Double.valueOf(((list.get((list.size()-1)/2))+((list.get(((list.size()-1)/2)+1)) )/2 )));

        }
        return median;
    }

    @Override
    public List<List<String>> frekuensi(List<Integer> list) {
        List<List<String>>frekuensi=new ArrayList<>();

        int count=1;
        int pos=0;
        for (int j = pos+1; j <list.size() ; j++) {
            List<String>tmpList=new ArrayList<>();
          if (list.get(pos)==list.get(j)) {
              count=count+1;
          }else {
              tmpList.add(String.valueOf(list.get(pos)));
              tmpList.add(String.valueOf(count));
              frekuensi.add(tmpList);
              pos=j;
              count=1;

          }
          if (j==list.size()-1){
              j=list.size();
              tmpList.add(String.valueOf(list.get(pos)));
              tmpList.add(String.valueOf(count));
              frekuensi.add(tmpList);

          }

        }
        return frekuensi;


    }



    @Override
    public String inputan() {
        Scanner scn = new Scanner(System.in);
        String x=scn.next();
        return x;
    }

    @Override
    public List<Integer> shorted (List<Integer> list) {
        Collections.sort(list);
        return list;
    }

    @Override
    public List<Integer> nilaiInteger(List<List<String>> list) {
        List<Integer>nilaiInteger=new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            for (int j = 1; j < list.get(i).size(); j++) {
                nilaiInteger.add(Integer.valueOf(String.valueOf(list.get(i).get(j))));
            }
        }
        return nilaiInteger;

    }

}
