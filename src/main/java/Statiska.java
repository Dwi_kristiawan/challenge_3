import java.util.List;

public interface Statiska {
    //statistika

    String modus(List<List<String>>list);
    String mean(List<Integer>list);
    String median(List<Integer>list);
    List<List<String>> frekuensi(List<Integer>list);


    //pengurutan data integer
    List<Integer> shorted(List<Integer>list);
    //pembentukan list nilai akumulasi ke integer
    List<Integer> nilaiInteger(List<List<String>>list);
    //inputan
    String inputan();

}
