import org.jetbrains.annotations.NotNull;

public class MenuImplement implements Menu {
    //inisialisasi implementasi statistika
    StatistikaImplement st = new StatistikaImplement();
    Generate generate=new GenerateImplement();
    @Override
    //Switch menu pada home
    public void switchMenuHome(String x) {
        switch (x){
            case"0":
                break;
            case"1":
                generate.generateTodoc("src/dataRecord/Frekuensi.txt"," ");
                printDetail();
                switchMenudetail(String.valueOf(st.inputan()));
                break;
            case"2":
                generate.generateTodoc("src/ModusMeanMedian.txt");
                printDetail();
                switchMenudetail(String.valueOf(st.inputan()));
                break;
            case"3":
                generate.generateTodoc("src/dataRecord/Frekuensi.txt"," ");
                generate.generateTodoc("src/ModusMeanMedian.txt");
                printDetail();
                switchMenudetail(String.valueOf(st.inputan()));
                break;
            default:
                System.out.println("Inputan tidak sesuai");
                perintHome();
                break;
        }

    }

    @Override
    public void switchMenudetail(@NotNull String x) {
        switch (x) {
            case "0":
                break;
            case "1":
                perintHome();
                break;
            default:
                System.out.println("Inputan tidak sesuai");
                perintHome();
                break;
        }
    }

    @Override
    public void perintHome() {
        System.out.println("_______________________________________________________________");
        System.out.println("Aplikasi Pengolah Nilai Siswa");
        System.out.println("_______________________________________________________________");
        System.out.println("Letakkan File csv dengan nama data_sekolah di direktori berikut");
        System.out.println("Directori:src");
        System.out.println();
        System.out.println("Pilih Menu");
        System.out.println("1. Generate txt untuk menampilkan nilai pengelompokan Frekuensi");
        System.out.println("2. Generate txt untuk menampilkan nilai Rata-rata dan Median ");
        System.out.println("3. Generate keduanya");
        System.out.println("0. exit");

        switchMenuHome(st.inputan());
    }

    @Override
        public void printDetail() {
        System.out.println("_______________________________________________________________");
        System.out.println("Aplikasi Pengolah Nilai Siswa");
        System.out.println("_______________________________________________________________");
        System.out.println("File telah di generate pada directori: src");
        System.out.println();
        System.out.println("0. exit");
        System.out.println("1. Kembali");
    }
}


