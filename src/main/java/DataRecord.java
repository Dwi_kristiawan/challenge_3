import java.util.ArrayList;
import java.util.List;

public class DataRecord extends StatistikaImplement{
    WriteFile wf=new WriteImplement();

    void record(List<List<String>>list){
        // inisialisasi penyimpanan sementara
        List<Integer>nilaiIntegerTmp=new ArrayList<>();
        List<Integer>shortedTmp=new ArrayList<>();
        String meanTmp;
        String medianTmp;
        String modusTmp;
        List<List<String>>frekuensiTmp=new ArrayList<>();

        //pemindahan data dari pemindai ke penyimpanan sementara
        nilaiIntegerTmp=nilaiInteger(list);
        shortedTmp=shorted(nilaiIntegerTmp);
        frekuensiTmp=frekuensi(shortedTmp);
        meanTmp=mean(shortedTmp);
        medianTmp=median(shortedTmp);
        modusTmp=modus(frekuensiTmp);

        // menyimpan data frekuensi, mean, median , modus dalam bentuk file
        wf.writing(meanTmp,"Mean.txt");
        wf.writing(medianTmp,"Median.txt");
        wf.writing(modusTmp,"Modus.txt");
        wf.writing(frekuensiTmp,"Frekuensi.txt");

    }
}
