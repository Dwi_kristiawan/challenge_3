import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class GenerateImplement implements Generate{


    @Override
    public void generateTodoc(String filePath, String delimiter) {
        try {
            //readfile
            File file = new File(filePath);
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String line = "";
            String[] arr;
            //writefile
            String fileName = "src/Frekuensi.txt";
            FileWriter fileWriter = new FileWriter(fileName);
            fileWriter.write("Berikut Hasil Pengolahan Nilai"+'\n');
            fileWriter.write("   Nilai    |    Frekuensi"+'\n');
            while ((line = br.readLine()) != null) {

                arr = line.split(delimiter);
                for (String str : arr
                ) {

                    fileWriter.write("    "+str+"         ");

                }
                fileWriter.write("\n");

            }
            br.close();
            fr.close();
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Terjadi kesalahan karena: " + e.getMessage());
        }
    }

    @Override
    public void generateTodoc(String fileName) {
        try {

            //writefile
            FileWriter fileWriter = new FileWriter(fileName);
            fileWriter.write("Berikut Hasil Pengolahan Nilai"+'\n');
            fileWriter.write("Berikut Hasil Sebaran Nilai"+'\n'+'\n');
            //readfile
            File file = new File("src/dataRecord/Modus.txt");
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String line = "";
            String barisData;
            while((barisData = br.readLine())!= null){
                fileWriter.write("Modus   : "+barisData);
                break;
            }
            fileWriter.write("\n");
            file=new File("src/dataRecord/Mean.txt");
            fr= new FileReader(file);
            br = new BufferedReader(fr);
            while((barisData = br.readLine())!= null){
                fileWriter.write("Mean    : "+barisData);
                break;
            }
            fileWriter.write("\n");
            file=new File("src/dataRecord/Median.txt");
            fr= new FileReader(file);
            br = new BufferedReader(fr);
            while((barisData = br.readLine())!= null){
                fileWriter.write("Median : "+barisData);
                break;
            }
            fileWriter.close();
            br.close();
            fr.close();


        } catch (IOException e) {
            System.out.println("Terjadi kesalahan karena: " + e.getMessage());
        }
    }
}
