import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Statistik {

    //Mean
    public static void avgtotal(){
        float tmp=0;
        for (int i = 0; i < Recorder.akumulasiNilai.size(); i++) {
            tmp=tmp+ Recorder.akumulasiNilai.get(i);
        }
        Recorder.avgKomulatif=String.valueOf(tmp/ Recorder.akumulasiNilai.size());
    }

    //median
    public static void median(){
        Shorter.shorting(Recorder.akumulasiNilai);
        Float medianTmp;
        if(Recorder.akumulasiNilai.size()%2==0){
            medianTmp=Float.valueOf(Recorder.akumulasiNilai.get(Recorder.akumulasiNilai.size()/2)+(Recorder.akumulasiNilai.get(Recorder.akumulasiNilai.size()/2)+1))/2;
        }else {
            medianTmp=Float.valueOf(Recorder.akumulasiNilai.get(Recorder.akumulasiNilai.size()/2)+1);
        }
        Recorder.median=String.valueOf(medianTmp);
    }


    //frekuensi
   public static void frekuensi(){
       Shorter.shorting(Recorder.akumulasiNilai);
        List<List<String>>list=new ArrayList<>();
       int i = 1;
       int tmp=1;
       for (int j = i+1; j <Recorder.akumulasiNilai.size() ; j++) {
           List<String>listTmp = new ArrayList<>();
           int a=(Recorder.akumulasiNilai.get(i));
           int b=(Recorder.akumulasiNilai.get(j));
           if (a==b){
                   tmp=tmp+1;
           }else{
               tmp=tmp+1;
               listTmp.add(String.valueOf(Recorder.akumulasiNilai.get(i)));
               listTmp.add(String.valueOf(tmp));
               list.add(listTmp);
               i=j;
               tmp=0;
           }
           if(j==Recorder.akumulasiNilai.size()-1){
               listTmp.add(String.valueOf(Recorder.akumulasiNilai.get(i)));
               listTmp.add(String.valueOf(Recorder.akumulasiNilai.get(i)));
               list.add(listTmp);
               i=Recorder.akumulasiNilai.size();
           }
       }
       Recorder.frequensi=list;
   }
   public static void modus(){
       frekuensi();
       String tmpFrequensi="0";
       String tmpNilai="0";
       for (int j = 0; j <Recorder.frequensi.size(); j++) {
            if (Integer.parseInt(tmpFrequensi)<Integer.parseInt(Recorder.frequensi.get(j).get(1))){
                tmpFrequensi=Recorder.frequensi.get(j).get(1);
                tmpNilai=Recorder.frequensi.get(j).get(0);
            }
       }
       Recorder.Modus=tmpNilai;
   }

}

