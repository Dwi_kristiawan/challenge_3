import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class ReadFileImplement implements ReadFile{
    //inisilaisasi datarecord.
    DataRecord dr=new DataRecord();

    @Override
    public void readFile(String filePath, String delimiter) {
        List<List<String>> listNilai = new ArrayList<>();
        try {
            File file = new File(filePath);
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String line = "";
            String[] arr;

            while ((line = br.readLine()) != null) {
                List<String> nilaiKelas = new ArrayList<>();
                arr = line.split(delimiter);
                for (String str : arr
                ) {

                    nilaiKelas.add(str);// untuk satu list kelas

                }
                listNilai.add(nilaiKelas);
            }
            br.close();
            fr.close();
            dr.record(listNilai);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
